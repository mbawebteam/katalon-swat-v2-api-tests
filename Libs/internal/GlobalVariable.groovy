package internal

import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase

/**
 * This class is generated automatically by Katalon Studio and should not be modified or deleted.
 */
public class GlobalVariable {
     
    /**
     * <p></p>
     */
    public static Object G_Cookie
     
    /**
     * <p></p>
     */
    public static Object G_Host
     

    static {
        def allVariables = [:]        
        allVariables.put('default', ['G_Cookie' : '__RequestVerificationToken=pZLq5l0_UydKvbnQqXfmss3R372EXbVSeHn720rvWyav_aff-auKGBKihfCPjj3P6BL8APLNJ4vxbUTuvSWRGX2MPfqKwkJkB91mWbCBVS81; ASP.NET_SessionId=rfie4vhhe4yslb3zncm5plvr', 'G_Host' : 'qa.seafoodwatchassessments.org'])
        
        String profileName = RunConfiguration.getExecutionProfile()
        def selectedVariables = allVariables[profileName]
		
		for(object in selectedVariables){
			String overridingGlobalVariable = RunConfiguration.getOverridingGlobalVariable(object.key)
			if(overridingGlobalVariable != null){
				selectedVariables.put(object.key, overridingGlobalVariable)
			}
		}

        G_Cookie = selectedVariables["G_Cookie"]
        G_Host = selectedVariables["G_Host"]
        
    }
}
