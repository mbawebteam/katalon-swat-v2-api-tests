
/**
 * This class is generated automatically by Katalon Studio and should not be modified or deleted.
 */

import com.kms.katalon.core.testobject.ResponseObject


def static "delete.ParseResponse.parseFirstRecord"(
    	ResponseObject input	) {
    (new delete.ParseResponse()).parseFirstRecord(
        	input)
}
