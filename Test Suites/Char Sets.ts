<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description>This is related to SWATV2-431 and SWATV2-183</description>
   <name>Char Sets</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>a9227770-4c90-4932-8ee3-0582372bde57</testSuiteGuid>
   <testCaseLink>
      <guid>775d0676-43be-472a-86e4-530bc2205954</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Users Table/MBA_AddUser Japanese Characters</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bd11256c-1a7a-4e60-aee7-ac102879183c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Users Table/MBA_DelUser Japanese Characters</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fd9cda2b-2fad-4e54-8b5c-24125a75368b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Users Table/MBA_AddUser Latin Characters</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>aae004e7-1ac6-466c-b827-d3e0df1f47eb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Users Table/MBA_DelUser latin Characters</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d39a1055-cbf7-4cc2-b94c-3f9f35a3a600</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Methods/MBA_AddMethod Japanese Characters</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d0e402eb-ec1c-4206-8621-a20b334a3f1a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Methods/MBA_DelMethod Japanese Characters</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5cc46f21-b277-48ed-a5c4-8ef49a2881b6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Methods/MBA_AddMethod Latin Characters</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>87c9d06e-c909-4c4e-844a-c3dbb18598fd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Methods/MBA_DelMethod Latin Characters</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
