import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import groovy.json.JsonSlurper as JsonSlurper


// Get the new created user, and verify the reponse shows the japanese characters
def Response = WS.sendRequest(findTestObject('Users Table/MBA_ListUsers Latin Characters', [('Cookie') : GlobalVariable.G_Cookie
            , ('Host') : GlobalVariable.G_Host, ('UserName') : 'TestLatinChar']))
WS.verifyResponseStatusCode(Response, 200)

//This custom keyword parses the response and returns the id.
def responseData = CustomKeywords.'delete.ParseResponse.parseFirstRecord'(Response)
println "data Id: " + responseData.Id

//Verify by username that the correct record was listed.
WS.verifyElementPropertyValue(Response, 'ResultSets[0][0].UserName', 'TestLatinChar')

//Delete the same record and verify success.
def DeleteResponse = WS.sendRequest(findTestObject('Object Repository/Users Table/MBA_DelUser latin Characters', [('Cookie') : GlobalVariable.G_Cookie
           , ('Host') : GlobalVariable.G_Host, ('Id') : responseData.Id]))

WS.verifyResponseStatusCode(DeleteResponse, 200)

