package delete

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import groovy.json.JsonSlurper as JsonSlurper

import internal.GlobalVariable
//This custom keyword parses out the first record of the api response.
//When adding this custom keyword to a test script, save this keyword to a variable, i.e.,
//"responseData". Then, when you want to call a specific field, do it like so: "responseData.Id". See example at
// "Test Cases/Users Table/MBA_DelUser Japanese Characters".

public class ParseResponse {

	@Keyword
	def static parseFirstRecord(ResponseObject input){

		//Code to parse the Response
		def jsonSlurper = new JsonSlurper()

		def parsed_Response = jsonSlurper.parseText(input.getResponseText())
		println "json: " + parsed_Response

		//Parse the Result Set layer of the response array.
		def parsedRS = parsed_Response.ResultSets
		println "parsed ResultSets: " + parsedRS

		//Return the first record data.
		return parsedRS[0][0]
	}
}
