<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>MBA_Add User with latin Characters</name>
   <tag></tag>
   <elementGuidId>359f54a3-fdf9-4c51-924d-58838f59fefb</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;{\n\&quot;updatePasswordCheck\&quot;: \&quot;true\&quot;,\n\&quot;IsAdmin\&quot;: \&quot;false\&quot;,\n\&quot;Password\&quot;: ,\n\&quot;FirstName\&quot;:\&quot;ħñĨĩ\&quot;,\n\&quot;LastName\&quot;:\&quot;ĨĩĪ\&quot;,\n\&quot;UserName\&quot;:\&quot;TestLatinChar\&quot;,\n\&quot;DisplayName\&quot;:\&quot;ĪīĬ\&quot;,\n\&quot;Organization\&quot;:\&quot;ĬĭĮ\&quot;,\n\&quot;OrganizationType\&quot;: \&quot;1\&quot;,\n\&quot;PasswordHash\&quot;:\&quot;ĸĹĺ\&quot;,\n\&quot;PasswordEncrypted\&quot;:\&quot;ĹĺĻ\&quot;,\n\&quot;Email\&quot;:\&quot;a@a.com\&quot;,\n\&quot;IsAuthor\&quot;: \&quot;false\&quot;,\n\&quot;IsReviewer\&quot;: \&quot;false\&quot;,\n\&quot;IsPeerReviewer\&quot;: \&quot;false\&quot;,\n\&quot;IsPhd\&quot;: \&quot;false\&quot;,\n\&quot;IsMasters\&quot;: \&quot;false\&quot;,\n\&quot;IsStaff\&quot;: \&quot;false\&quot;,\n\&quot;IsPresentStaff\&quot;: \&quot;false\&quot;,\n\&quot;IsPastStaff\&quot;: \&quot;false\&quot;,\n\&quot;Address1\&quot;:\&quot;įİı\&quot;,\n\&quot;Address2\&quot;:\&quot;İıĲ\&quot;,\n\&quot;City\&quot;:\&quot;ıĲĳ\&quot;,\n\&quot;State\&quot;:\&quot;ĲĳĴ\&quot;,\n\&quot;Zip\&quot;:\&quot;ĳĴĵ\&quot;,\n\&quot;Country\&quot;:\&quot;ĴĵĶ\&quot;,\n\&quot;Phone\&quot;:\&quot;ĵĶķ\&quot;,\n\&quot;Fax\&quot;:\&quot;Ķķĸ\&quot;,\n\&quot;Notes\&quot;:\&quot;ķĸĹ\&quot;,\n\&quot;IsDeleted\&quot;: \&quot;false\&quot;,\n\&quot;IsNgo\&quot;: \&quot;false\&quot;,\n\&quot;SecurityStamp\&quot;:\&quot;ĹĺĻ\&quot;,\n\&quot;UsersOrganizationId\&quot;: ,\n\&quot;LocationId\&quot;:, \n\&quot;Status\&quot;: \&quot;0\&quot;\n}&quot;,
  &quot;contentType&quot;: &quot;application/json&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Cookie</name>
      <type>Main</type>
      <value>${Cookie}</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>https://${Host}/sqldev/MBA_AddUser?</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <variables>
      <defaultValue>GlobalVariable.G_Cookie</defaultValue>
      <description></description>
      <id>4901a88f-058f-4c4f-a92d-830c19f7ba9d</id>
      <masked>false</masked>
      <name>Cookie</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.G_Host</defaultValue>
      <description></description>
      <id>acba65fa-0ace-4490-918f-5e64b369c4b3</id>
      <masked>false</masked>
      <name>Host</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()
</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
