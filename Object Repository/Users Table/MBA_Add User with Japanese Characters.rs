<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>MBA_Add User with Japanese Characters</name>
   <tag></tag>
   <elementGuidId>e1aa00d8-700a-42ac-8617-dff6b2757ff0</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;{\n\&quot;updatePasswordCheck\&quot;: \&quot;true\&quot;,\n\&quot;IsAdmin\&quot;: \&quot;false\&quot;,\n\&quot;Password\&quot;: ,\n\&quot;FirstName\&quot;: \&quot;Test Japanese1\&quot;,\n\&quot;LastName\&quot;: \&quot;Test 𠮟\&quot;,\n\&quot;UserName\&quot;: \&quot;TestJapaneseChar\&quot;,\n\&quot;DisplayName\&quot;: \&quot;撹\&quot;,\n\&quot;Organization\&quot;: \&quot;掴\&quot;,\n\&quot;OrganizationType\&quot;: \&quot;1\&quot;,\n\&quot;PasswordHash\&quot;: ,\n\&quot;PasswordEncrypted\&quot;: \&quot;S3aott3r\&quot;,\n\&quot;Email\&quot;: \&quot;japanese1@mbayaq.org\&quot;,\n\&quot;IsAuthor\&quot;: \&quot;false\&quot;,\n\&quot;IsReviewer\&quot;: \&quot;false\&quot;,\n\&quot;IsPeerReviewer\&quot;: \&quot;false\&quot;,\n\&quot;IsPhd\&quot;: \&quot;false\&quot;,\n\&quot;IsMasters\&quot;: \&quot;false\&quot;,\n\&quot;IsStaff\&quot;: \&quot;false\&quot;,\n\&quot;IsPresentStaff\&quot;: \&quot;false\&quot;,\n\&quot;IsPastStaff\&quot;: \&quot;false\&quot;,\n\&quot;Address1\&quot;: ,\n\&quot;Address2\&quot;: ,\n\&quot;City\&quot;: ,\n\&quot;State\&quot;: ,\n\&quot;Zip\&quot;: ,\n\&quot;Country\&quot;: ,\n\&quot;Phone\&quot;: ,\n\&quot;Fax\&quot;: ,\n\&quot;Notes\&quot;: ,\n\&quot;IsDeleted\&quot;: \&quot;false\&quot;,\n\&quot;IsNgo\&quot;: \&quot;false\&quot;,\n\&quot;SecurityStamp\&quot;: ,\n\&quot;UsersOrganizationId\&quot;: ,\n\&quot;LocationId\&quot;:, \n\&quot;Status\&quot;: \&quot;0\&quot;\n}&quot;,
  &quot;contentType&quot;: &quot;application/json&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Cookie</name>
      <type>Main</type>
      <value>${Cookie}</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>https://${Host}/sqldev/MBA_AddUser?</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <variables>
      <defaultValue>GlobalVariable.G_Cookie</defaultValue>
      <description></description>
      <id>4901a88f-058f-4c4f-a92d-830c19f7ba9d</id>
      <masked>false</masked>
      <name>Cookie</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.G_Host</defaultValue>
      <description></description>
      <id>acba65fa-0ace-4490-918f-5e64b369c4b3</id>
      <masked>false</masked>
      <name>Host</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()
</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
