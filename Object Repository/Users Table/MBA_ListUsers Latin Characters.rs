<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description>This test gets the record that was just posted that contains Japanese Characters</description>
   <name>MBA_ListUsers Latin Characters</name>
   <tag></tag>
   <elementGuidId>95917ef5-1f14-45c4-8d63-1638132d092c</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <httpBody></httpBody>
   <httpBodyContent></httpBodyContent>
   <httpBodyType></httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Cookie</name>
      <type>Main</type>
      <value>${Cookie}</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>GET</restRequestMethod>
   <restUrl>https://${Host}/sqldev/MBA_ListUsers?UserName=${UserName}</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <variables>
      <defaultValue>GlobalVariable.G_Cookie</defaultValue>
      <description></description>
      <id>352b039f-cd23-4ca2-b5a7-c8cb6e12c6b3</id>
      <masked>false</masked>
      <name>Cookie</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.G_Host</defaultValue>
      <description></description>
      <id>e4d89218-f890-4926-afe5-0a44db2aa087</id>
      <masked>false</masked>
      <name>Host</name>
   </variables>
   <variables>
      <defaultValue>'TestLatinChar'</defaultValue>
      <description></description>
      <id>acba2fdd-61bb-4d74-9fb6-3c695b970c9b</id>
      <masked>false</masked>
      <name>UserName</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()
</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
